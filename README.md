# mysqlimagereplacer

mySQL image downloader / replacer wizard

v.0.3 beta adopted for php8

DWTWL license https://soundragon.su/license/license.html

This is beta version written for specific needs. If you want complete and expanded version of this script, then make the request.



!MAKE BACKUP OF YOUR SQL-TABLE BEFORE YOU TRY THIS SCRIPT!

requirement: php-xml module

Using:

1. Check php-xml is installed.
2. Copy files to new subdirectory of your web-server, for example:
/mysqlim/
3. Add write privilegies for this directory.
4. Login to wizard by web-browser:
http://yourserver.com/mysqlim/index.php
