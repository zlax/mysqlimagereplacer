<?php
/**
* mySQL image replacer - https://gitlab.com/zlax/mysqlimagereplacer
* adopted for php8.2
* v.0.3 beta - uder DWTWL license https://soundragon.su/license/license.html
* interface script
*
* !MAKE BACKUP OF YOUR SQL-TABLE BEFORE YOU TRY THIS SCRIPT!
*
* requirement: php-xml module
*
* todo: curl and $wgetline="wget --timeout=2 http://somedomain/image.jpg";
* todo: rawurlencode for non-latin domains
*/
session_start();
if (!isset($_SESSION["template"])) $_SESSION["template"]="Custom";

function PageView() {
// First wizard-page view
    if ($_SESSION["step"]==0) {
        echo "STEP 1: choose database:<br>\n";
        $_SESSION["subdir"]="images";
        echo "<form action=index.php method=post>\n";
        echo "  database host: <input type=text name=\"dbhost\" value=\"localhost\" maxlength=255><br>\n";
        echo "  database user: <input type=text name=\"dbuser\" value=\"",$_SESSION["dbuser"] ?? null,"\" maxlength=255><br>\n";
        echo "  database pass: <input type=password name=\"dbpass\" maxlength=255><br>\n";
        echo "  database name: <input type=text name=\"dbname\" value=\"",$_SESSION["dbname"] ?? null,"\" maxlength=255><br>\n";
        echo "  type template: <select name=\"dbtype\">\n";
        echo "  <option value=\"Custom\"";
            if (($_SESSION["template"]=="Custom")||(!$_SESSION["template"])) echo " selected";
        echo ">Custom</option>\n";
        echo "  <option value=\"Wordpress\"";
            if ($_SESSION["template"]=="Wordpress") echo " selected";
        echo ">Wordpress</option>\n";
        echo "  <option value=\"Joomla\"";
            if ($_SESSION["template"]=="Joomla") echo " selected";
        echo ">Joomla</option>\n";
        echo "  <option value=\"Drupal\"";
            if ($_SESSION["template"]=="Drupal") echo " selected";
        echo ">Drupal</option>\n";
        echo "  </select><br>\n";
        echo "  <input type=submit name=\"next0\" value=\"Next\">\n";
        echo "</form>\n";
        if ($_SESSION["error"]=="dbconnection") echo "  Error: ", mysqli_connect_error(),"<br>\n";
        if ($_SESSION["error"]=="dbconnectionname") echo "  Error: enter database name.<br>\n";
    }
// Second wizard-page view
    if ($_SESSION["step"]==1) {
        echo "STEP 2: entries and directories:<br>\n";
        echo "<form action=index.php method=post>\n";
        echo "  entries located in ",$_SESSION["dbname"],"@",$_SESSION["dbhost"],"<br>\n";
        echo "  Table name: <input type=text name=\"dbtable\" value=\"",$_SESSION["dbtable"],"\" maxlength=255><br>\n";
        echo "  Column name: <input type=text name=\"dbcolumn\" value=\"",$_SESSION["dbcolumn"],"\" maxlength=255><br>\n";
        echo "  Content directory: ~/<input type=text name=\"dirname\" value=\"",$_SESSION["dirname"],"\" maxlength=255><br>\n";
        echo "  Filter column: <input type=text name=\"IDcolumn\" value=\"",$_SESSION["IDcolumn"],"\" maxlength=255><br>\n";
        echo "  Filter column: <input type=text name=\"filtcolumn\" value=\"",$_SESSION["filtcolumn"],"\" maxlength=255><br>\n";
        echo "  Filter column value: <input type=text name=\"filtvalue\" value=\"",$_SESSION["filtvalue"],"\" maxlength=255><br>\n";
        echo "  <i>(F.e. to filter posts from drafts and attachments in Wordpress6+)</i><br>\n";
        echo "  <input type=submit name=\"prev1\" value=\"Previous\"> <input type=submit name=\"next1\" value=\"Next\">\n";
        echo "</form>\n";
        if ($_SESSION["error"]) echo $_SESSION["error"],"<br>\n";
    }
// Third wizard-page view
    if ($_SESSION["step"]==2) {
        echo "STEP 3: check data:<br>\n";
        $postnumber=0;
        $filteredpostnumber=0;
        $totalimagenumber=0;
        $_SESSION["correctimagenumber"]=0;
        $_SESSION["alreadyimagenumber"]=0;
        $_SESSION["brokenimagenumber"]=0;
        $dbconnection=mysqli_connect($_SESSION["dbhost"],$_SESSION["dbuser"],$_SESSION["dbpass"],$_SESSION["dbname"]);
        mysqli_set_charset($dbconnection,'utf8');
        $resulttable=mysqli_query($dbconnection,"SELECT * FROM ".$_SESSION["dbtable"]);
        $resultrows=mysqli_num_rows($resulttable);
        if (!empty($_SESSION["IDcolumn"]))
            $_SESSION["imgsrcsID"]=array();
        $_SESSION["imgsrcs"]=array();
        $_SESSION["imgsrcahrefs"]=array();
// -- Checking table
        while($row=mysqli_fetch_assoc($resulttable)) {
            $contentpost=new DOMDocument();
            libxml_use_internal_errors(true);
            if (!empty($_SESSION["filtcolumn"]) && !empty($_SESSION["filtvalue"])) {
                if ($row[$_SESSION["filtcolumn"]] == $_SESSION["filtvalue"]) {
                    if (!empty($row[$_SESSION["dbcolumn"]]))
                        $contentpost->loadHTML($row[$_SESSION["dbcolumn"]]);
                    $filteredpostnumber++;
                }
            } else {
                if (!empty($row[$_SESSION["dbcolumn"]]))
                    $contentpost->loadHTML($row[$_SESSION["dbcolumn"]]);
                $filteredpostnumber++;
            }
            $imageTags=$contentpost->getElementsByTagName('img');
            $ahrefImageTags=$imageTags;
            $imagenumberinpost=0;
            foreach($imageTags as $tag) {
                $imagenumberinpost++;
                $totalimagenumber++;
                if (!empty($_SESSION["IDcolumn"]))
                    $_SESSION["imgsrcsID"][$postnumber]=$row[$_SESSION["IDcolumn"]];
                $_SESSION["imgsrcs"][$postnumber][$imagenumberinpost]=$tag->getAttribute('src');
                $aofimg=$contentpost->importNode($tag->parentNode, true);
                if (isset($aofimg)) {
                    $ahreofimg=$aofimg->getAttribute('href');
                    if ($ahreofimg=="") $ahreofimg=$tag->getAttribute('src');
                    $_SESSION["imgsrcahrefs"][$postnumber][$imagenumberinpost]=$ahreofimg;
                }
                else
                    $_SESSION["imgsrcahrefs"][$postnumber][$imagenumberinpost]=$tag->getAttribute('src');
                $testurl=parse_url($_SESSION["imgsrcs"][$postnumber][$imagenumberinpost]);
                if ((!empty($testurl['host'])&&!empty($testurl['path']))&&(($testurl['scheme']=="http")||($testurl ['scheme']=="https"))&&(!($testurl['host']==$_SERVER['SERVER_NAME'])))
                    $_SESSION["correctimagenumber"]++;
                else if (isset($testurl['host']) && ($testurl['host']==$_SERVER['SERVER_NAME']))
                    $_SESSION["alreadyimagenumber"]++;
                else $_SESSION["brokenimagenumber"]++;
            }
            libxml_clear_errors();
            $postnumber++;
        }
        echo "<form action=index.php method=post>\n";
        echo "  directory to upload new images:<br>\n";
        echo "  ",getcwd(),"/<input type=text name=\"subdir\" value=\"",$_SESSION["subdir"],"\" maxlength=63> (leave blank if you don't need subdir)<br>\n";
        echo "  start processing from raw number: <input type=text name=\"fromnum\" value=\"",$_SESSION["fromnum"],"\" maxlength=63> (leave blank if all)<br>\n";
        echo "  type of html-prefix:<br>\n";
        echo "  <input type=\"radio\" name=\"htmlprefix\" value=\"http\"";
        if (!isset($_SESSION["htmlprefix"])) $_SESSION["htmlprefix"]="wo";
        if ($_SESSION["htmlprefix"]=="http") echo " checked";
        echo ">http://<br>\n";
        echo "  <input type=\"radio\" name=\"htmlprefix\" value=\"https\"";
        if ($_SESSION["htmlprefix"]=="https") echo " checked";
        echo ">https://<br>\n";
        echo "  <input type=\"radio\" name=\"htmlprefix\" value=\"wo\"";
        if ($_SESSION["htmlprefix"]=="wo") echo " checked";
        echo ">without prefix and domain name<br>\n";
        echo "  type of download:<br>\n";
        echo "  <input type=\"radio\" name=\"dwnldtype\" value=\"put\"";
        if (!isset($_SESSION["downloadtype"])) $_SESSION["downloadtype"]="put";
        if ($_SESSION["downloadtype"]=="put") echo " checked";
        echo ">file_put_contents<br>\n";
        echo "  <input type=\"radio\" name=\"dwnldtype\" value=\"curl\"";
        if ($_SESSION["downloadtype"]=="curl") echo " checked";
        echo " disabled"; // unsupported yet
        echo ">curl <i>[unsupported yet]</i><br>\n";
        echo "  <input type=\"radio\" name=\"dwnldtype\" value=\"wget\"";
        if ($_SESSION["downloadtype"]=="wget") echo " checked";
        echo " disabled"; // unsupported yet
        echo ">wget <i>[unsupported yet]</i><br>\n";
        echo "  number of rows in table: $resultrows<br>\n";
        echo "  filtered rows: $filteredpostnumber<br>\n";
        echo "  number of rows with imgsrc tags: ",count($_SESSION["imgsrcs"]),"<br>\n";
        echo "  total number of img src tags: $totalimagenumber<br>\n";
        echo "  broken image links: ",$_SESSION["brokenimagenumber"],"<br>\n";
        echo "  already downloaded image links: ",$_SESSION["alreadyimagenumber"],"<br>\n";
        echo "  correct external image links for download: ",$_SESSION["correctimagenumber"],"<br>\n";
        echo " <input type=checkbox name=\"ahrefparents\" value=\"yes\"";
        if (!isset($_SESSION["ahrefparents"])) $_SESSION["ahrefparents"]=true;
        if ($_SESSION["ahrefparents"]) echo " checked";
        echo ">download parent a hrefs if exists?<br>\n";
        echo "Domains for webarchive download, comma separated: <input type=text name=\"webarchived\" value=\"",$_SESSION["webarchived"],"\" maxlength=127> (leave blank if you don't need to download images from webarchive)<br>\n";
        echo " <input type=checkbox name=\"fixbroken\" value=\"yes\"";
        if (!isset($_SESSION["fixbroken"])) $_SESSION["fixbroken"]=false;
        if ($_SESSION["fixbroken"]) echo " checked";
        echo ">delete broken tags?<br>\n";
        echo " <input type=checkbox name=\"fixundownload\" value=\"yes\"";
        if (!isset($_SESSION["fixundownload"])) $_SESSION["fixundownload"]=false;
        if ($_SESSION["fixundownload"]) echo " checked";
        echo ">remove undownloadble tags?<br>\n";
        echo " <input type=checkbox name=\"fixalready\" value=\"yes\"";
        if (!isset($_SESSION["fixalready"])) $_SESSION["fixalready"]=false;
        if ($_SESSION["fixalready"]) echo " checked";
        echo " disabled"; // unsupported yet
        echo ">fix prefix to already downloaded tags? <i>[unsupported yet]</i><br>\n";
        echo "  <input type=submit name=\"prev2\" value=\"Previous\"> <input type=submit name=\"deta2\" value=\"Details\"> <input type=submit name=\"next2\" value=\"Next\">\n";
        echo "</form>\n";
        if ($_SESSION["error"]) echo $_SESSION["error"],"<br>\n";
        if ($_SESSION["correctimagenumber"]<1) echo "you dont have any external images to download or fix<br>\n";
        if ($_SESSION["details"]=="step2") {
            echo "green links will be try to downloaded, yellow (already downloaded) and red (broken) - not:<hr>\n";
            foreach ($_SESSION["imgsrcs"] as $postnum=>$postimgs) {
                echo ($postnum+1);
                if (!empty($_SESSION["IDcolumn"]))
                    echo " (ID ", $_SESSION["imgsrcsID"][$postnum],")";
                echo ":<br>\n";
                foreach ($postimgs as $imgnum=>$imgs) {
                    $testurl=parse_url($imgs);
                    if ((!empty($testurl['host'])&&!empty($testurl['path']))&&(($testurl['scheme']=="http")||($testurl ['scheme']=="https"))&&(!($testurl['host']==$_SERVER['SERVER_NAME']))) echo "  <font color=green>";
                        else if (isset($testurl['host']) && ($testurl['host']==$_SERVER['SERVER_NAME']))
                            echo "  <font color=yellow>";
                        else echo "  <font color=red>";
                    echo rawurldecode(urldecode($imgs)),"</font>, (";
                    if ($_SESSION["imgsrcahrefs"][$postnum][$imgnum]==$_SESSION["imgsrcs"][$postnum][$imgnum])
                        echo "<i>";
                    else echo "<b>";
                    echo rawurldecode(urldecode($_SESSION["imgsrcahrefs"][$postnum][$imgnum]));
                    if ($_SESSION["imgsrcahrefs"][$postnum][$imgnum]==$_SESSION["imgsrcs"][$postnum][$imgnum])
                        echo "</i>";
                    else echo "</b>";
                    echo ")<br>\n";
                }
                echo "<hr>\n";
            }
        }
    }
// Approve page view
    if ($_SESSION["step"]==3) {
        echo "STEP 4: approve processing:<br>\n";
        echo "<form action=index.php method=post>\n";
        echo "  new img src tag will be like that:<br>\n";
        echo "  <i>\"http://",$_SERVER['SERVER_NAME'],"/",$_SESSION["dirname"];
        if ($_SESSION["subdir"]) echo "/",rawurlencode($_SESSION["subdir"]);
        echo "/newimage.jpg\"</i><br>\n";
        echo "  will be:<br>\n";
        if ($_SESSION["correctimagenumber"]>0) {
            echo $_SESSION["correctimagenumber"]," images uploaded, images placed to ",getcwd();
            if ($_SESSION["subdir"]) echo "/",$_SESSION["subdir"];
            echo "/<br>\n";
        }
        if ($_SESSION["ahrefparents"])
            echo "parent a hrefs will be downloaded (if exists)<br>\n";
        if ($_SESSION["webarchived"])
            echo "this domains will be downloaded via webarchive: ", $_SESSION["webarchived"], "<br>\n";
        if ($_SESSION["fixbroken"])
            echo $_SESSION["brokenimagenumber"]," tags will be deleted<br>\n";
        echo "  are you sure to proceed?<br>\n";
        echo "  <input type=submit name=\"prev3\" value=\"Previous\"> <input type=submit name=\"next3\" value=\"Next\">\n";
        echo "</form>\n";
    }
// Start page view
    if ($_SESSION["step"]==4) {
        $_SESSION["error"]="";
        echo "RESULT:<br>\n";
        if ($_SESSION["subdir"]) {
            if (!is_dir($_SESSION["subdir"]))
                mkdir($_SESSION["subdir"]);
            if (!chdir($_SESSION["subdir"]))
                $_SESSION["error"]="can't to operate with new directory";
        } else {
            if (mkdir ("testimuploaddir"))
                rmdir("testimuploaddir");
            else $_SESSION["error"]="can't to operate with directory, check privilegies";
        }
        if ($_SESSION["error"])
            echo $_SESSION["error"]."<br>\n";
        else {
            chdir(__DIR__);
            $csvsessiondata[]=$_SESSION["dbhost"];
            $csvsessiondata[]=$_SESSION["dbuser"];
            $csvsessiondata[]=$_SESSION["dbpass"];
            $csvsessiondata[]=$_SESSION["dbname"];
            $csvsessiondata[]=$_SESSION["dbtable"];
            $csvsessiondata[]=$_SESSION["dbcolumn"];
            $csvsessiondata[]=$_SERVER['SERVER_NAME'];
            $csvsessiondata[]=$_SESSION["fixbroken"];
            $csvsessiondata[]=$_SESSION["dirname"];
            $csvsessiondata[]=$_SESSION["subdir"];
            $csvsessiondata[]=$_SESSION["downloadtype"];
            $csvsessiondata[]=$_SESSION["htmlprefix"];
            $csvsessiondata[]=$_SESSION["fixalready"];
            $csvsessiondata[]=$_SESSION["fixundownload"];
            $csvsessiondata[]=$_SESSION["correctimagenumber"];
            $csvsessiondata[]=$_SESSION["filtcolumn"];
            $csvsessiondata[]=$_SESSION["filtvalue"];
            $csvsessiondata[]=$_SESSION["ahrefparents"];
            $csvsessiondata[]=$_SESSION["webarchived"];
            $csvsessiondata[]=$_SESSION["fromnum"];
            $fhandle=fopen("indata.csv","w");
            fputcsv($fhandle,$csvsessiondata);
            fclose($fhandle);
            exec("php -q exec.php < /dev/null > exec.log &");
            echo "Process started, it's take some time. Please press Details for check status<br>\n";
        }
        echo "<form action=index.php method=post>\n";
        echo "  <input type=submit name=\"deta4\" value=\"Details\">\n";
        echo "</form>\n";
    }
// Result page view
    if ($_SESSION["step"]==5) {
        echo "RESULT:<br>\n";
        if (file_exists("outdata.csv")) {
            $fhandle=fopen("outdata.csv","r");
            $outdatacsv=fgetcsv($fhandle, 1024, ",");
            fclose($fhandle);
            $rawsedit=$outdatacsv[0];
            $updatedurls=$outdatacsv[1];
            $failedtoupdate=$outdatacsv[2];
            $deletedurls=$outdatacsv[3];
            echo "raws edited: ",$rawsedit,", urldecode downloaded: ",$updatedurls,", failed to download: ",$failedtoupdate,", deleted tags: ",$deletedurls,"<br>\n";
        } else echo "no results<br>\n";
        if (file_exists("exec.log")) {
            $logexec=file_get_contents("exec.log");
            echo $logexec;
        } else echo "no log<br>\n";
        echo "<form action=index.php method=post>\n";
        echo "  <input type=submit name=\"deta5\" value=\"Details\">\n";
        echo "</form>\n";
    }
}
// First wizard-page handler
    $_SESSION["step"]=0;
    $_SESSION["error"]="";
    $_SESSION["details"]="";
    if (isset($_POST["next0"])) {
        $_SESSION["dbhost"]=$_POST["dbhost"];
        $_SESSION["dbuser"]=$_POST["dbuser"];
        $_SESSION["dbpass"]=$_POST["dbpass"];
        $_SESSION["dbname"]=$_POST["dbname"];
        $_SESSION["template"]=$_POST["dbtype"];
        $dbconnection=mysqli_connect($_SESSION["dbhost"],$_SESSION["dbuser"],$_SESSION["dbpass"],$_SESSION["dbname"]);
        $_SESSION["step"]=1;
        if (!$dbconnection) {
            $_SESSION["step"]=0;
            $_SESSION["error"]="dbconnection";
        }
        if ($_SESSION["template"]=="Wordpress") {
            $_SESSION["dbtable"]="wp_posts";
            $_SESSION["dbcolumn"]="post_content";
            $_SESSION["dirname"]="wp-content/uploads";
            $_SESSION["IDcolumn"]="ID";
            $_SESSION["filtcolumn"]="post_type";
            $_SESSION["filtvalue"]="post";
        }
        if ($_SESSION["template"]=="Joomla") {
            $_SESSION["dbtable"]="prefix_content";
            $_SESSION["dbcolumn"]="fulltext";
            $_SESSION["dirname"]="images/stories";
        }
        if ($_SESSION["template"]=="Drupal") {
            $_SESSION["dbtable"]="field_data_body";
            $_SESSION["dbcolumn"]="body_value";
            $_SESSION["dirname"]="sites/default/files";
        }
        if (!$_POST["dbname"]) {
            $_SESSION["step"]=0;
            $_SESSION["error"]="dbconnectionname";
        }
    }
// Second wizard-page handler
    if (isset($_POST["prev1"])) {
        $_SESSION["dbtable"]=$_POST["dbtable"];
        $_SESSION["dbcolumn"]=$_POST["dbcolumn"];
        $_SESSION["dirname"]=$_POST["dirname"];
        $_SESSION["IDcolumn"]=$_POST["IDcolumn"];
        $_SESSION["filtcolumn"]=$_POST["filtcolumn"];
        $_SESSION["filtvalue"]=$_POST["filtvalue"];
        $_SESSION["step"]=0;
    } if (isset($_POST["next1"])) {
        $_SESSION["dbtable"]=$_POST["dbtable"];
        $_SESSION["dbcolumn"]=$_POST["dbcolumn"];
        $_SESSION["dirname"]=$_POST["dirname"];
        $_SESSION["IDcolumn"]=$_POST["IDcolumn"];
        $_SESSION["filtcolumn"]=$_POST["filtcolumn"];
        $_SESSION["filtvalue"]=$_POST["filtvalue"];
        $dbconnection=mysqli_connect($_SESSION["dbhost"],$_SESSION["dbuser"],$_SESSION["dbpass"],$_SESSION["dbname"]);
        $columns=mysqli_query($dbconnection, "SHOW COLUMNS FROM ".$_SESSION["dbtable"]);
        if (!$columns) {
            $_SESSION["error"]=mysqli_error();
            $_SESSION["step"]=1;
        }
        if (mysqli_num_rows($columns)>0) {
            $_SESSION["error"]="Column not found";
            while ($row = mysqli_fetch_assoc($columns)) if ($row['Field']==$_SESSION["dbcolumn"]) $_SESSION["error"]="";
        } else $_SESSION["error"]="Table is empty or not exists";
        $uploadsdir=chdir("../".$_SESSION["dirname"]);
        if ($uploadsdir) {
            if (!is_writeable(getcwd())) $_SESSION["error"]="Cant write to directory, check previlegies";
        } else $_SESSION["error"]="Cant open directory";
        if (!$_SESSION["error"])
            $_SESSION["step"]=2;
        else $_SESSION["step"]=1;
    }
// Third wizard-page handler
    if (isset($_POST["prev2"])) {
        $_SESSION["step"]=1;
        $_SESSION["subdir"]=$_POST["subdir"];
    }
    if (isset($_POST["deta2"])) {
        $_SESSION["details"]="step2";
        $_SESSION["step"]=2;
        $_SESSION["subdir"]=$_POST["subdir"];
        $_SESSION["htmlprefix"]=$_POST["htmlprefix"];
        $_SESSION["downloadtype"]=$_POST["dwnldtype"];
        if (isset($_POST['fromnum']))
            $_SESSION["fromnum"]=$_POST["fromnum"];
        else $_SESSION["fromnum"]=false;
        if (isset($_POST['ahrefparents']) && ($_POST['ahrefparents'] == 'yes'))
            $_SESSION["ahrefparents"]=true;
        else $_SESSION["ahrefparents"]=false;
        if (isset($_POST['webarchived']))
            $_SESSION["webarchived"]=$_POST["webarchived"];
        else $_SESSION["webarchived"]=false;
        if (isset($_POST['fixbroken']) && ($_POST['fixbroken'] == 'yes'))
            $_SESSION["fixbroken"]=true;
        else $_SESSION["fixbroken"]=false;
        if (isset($_POST['fixundownload']) && ($_POST['fixundownload'] == 'yes'))
            $_SESSION["fixundownload"]=true;
        else $_SESSION["fixundownload"]=false;
        if (isset($_POST['fixalready']) && ($_POST['fixalready'] == 'yes'))
            $_SESSION["fixalready"]=true;
        else $_SESSION["fixalready"]=false;
        $uploadsdir=chdir("../".$_SESSION["dirname"]);
    }
    if (isset($_POST["next2"])) {
        $_SESSION["step"]=3;
        $_SESSION["subdir"]=$_POST["subdir"];
        $_SESSION["htmlprefix"]=$_POST["htmlprefix"];
        $_SESSION["downloadtype"]=$_POST["dwnldtype"];
        if (isset($_POST['fromnum']))
            $_SESSION["fromnum"]=$_POST["fromnum"];
        else $_SESSION["fromnum"]=false;
        if (isset($_POST['ahrefparents']) && ($_POST['ahrefparents'] == 'yes'))
            $_SESSION["ahrefparents"]=true;
        else $_SESSION["ahrefparents"]=false;
        if (isset($_POST['webarchived']))
            $_SESSION["webarchived"]=$_POST["webarchived"];
        else $_SESSION["webarchived"]=false;
        if (isset($_POST['fixbroken']) && ($_POST['fixbroken'] == 'yes'))
            $_SESSION["fixbroken"]=true;
        else $_SESSION["fixbroken"]=false;
        if (isset($_POST['fixundownload']) && ($_POST['fixundownload'] == 'yes'))
            $_SESSION["fixundownload"]=true;
        else $_SESSION["fixundownload"]=false;
        if (isset($_POST['fixalready']) && ($_POST['fixalready'] == 'yes'))
            $_SESSION["fixalready"]=true;
        else $_SESSION["fixalready"]=false;
        $uploadsdir=chdir("../".$_SESSION["dirname"]);
        if (isset($_POST['fixbroken']) && ($_POST['fixbroken'] == 'yes')) {
            if (($_SESSION["correctimagenumber"]<1)&($_SESSION["brokenimagenumber"]<1))
                $_SESSION["step"]=2;
        } else if ($_SESSION["correctimagenumber"]<1)
            $_SESSION["step"]=2;
    }
// Approve page handler
    if (isset($_POST["prev3"])) {
        $_SESSION["step"]=2;
        $uploadsdir=chdir("../".$_SESSION["dirname"]);
    }
    if (isset($_POST["next3"])) {
        $_SESSION["step"]=4;
        $uploadsdir=chdir("../".$_SESSION["dirname"]);
    }
// Start page handler
    if (isset($_POST["deta4"])) {
        $_SESSION["step"]=5;
    }
// Result page handler
    if (isset($_POST["deta5"])) {
        $_SESSION["step"]=5;
    }

?><!DOCTYPE html>
<html>
<head>
  <title>image replacer</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<?php PageView();?>
</body>
</html>
